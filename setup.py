import setuptools

with open("README.md") as fh:
    long_description=fh.read()


setuptools.setup(
    name="digivlasna",
    version="0.0.1",
    # scripts=[],
    author="asri djufri",
    author_email="asri@trinix.id",
    description="digivla sna core library",
    long_description = long_description,
    url="https://bitbucket.org/asrijuhas/digivlasna",
    packages=setuptools.find_packages(),
    install_requires=[
        'elasticsearch-dsl>=7.0.0,<8.0.0',
        'py2neo==4.3.0 '
    ],
    classifiers=[
        "Program language : python : 3.7 ",
        "Project: digivla sna"
    ]
)