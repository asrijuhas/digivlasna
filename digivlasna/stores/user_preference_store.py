from elasticsearch.exceptions import NotFoundError
from digivlasna.core.documents.user_preference import UserPreferenceDoc

class UserPreferenceDocStore():

    
    
    def save_user_preference(self, obj):
        obj.save()
        return obj

    def get_by_meta_id(self, user_uuid):
        try:
            doc = UserPreferenceDoc.get(id = user_uuid)
            return doc
        except NotFoundError as ex:
            return None
    
    

    
