from digivlasna.core.documents.post import IfapDoc
from digivlasna.core.constants import SOSMED_PLATFORMS
from elasticsearch.exceptions import NotFoundError
from digivlasna.core.exceptions import EntityValidationException
from datetime import datetime


class IfapDocStore():
    def create(self, obj):
        '''Method for save ifap doc to elastic

        :param obj: object dict ifap doc
        :return: success message
        '''
        check_doc = IfapDoc.get(name=obj["name_doc"])

        if check_doc:
            return {'Message': 'The document already exists!'}
        else:
            print(obj)
            # obj.save()
            return obj

    def delete(self, name_doc):
        '''Method for delete ifap doc to elastic

        :param name_doc: name_doc which want to delete
        :return: success message
        '''

        doc = IfapDoc.get(name=name_doc)
        if doc:
            doc.delete()

            return {'Message': '{} has been deleted!'.format(name_doc)}
        else:
            return {'Message': 'The document no longer exists!'}

    def update(self, obj, name_doc):
        '''Method for update the value ifap doc

        :param obj: new ifap doc
        :name_doc: name ifap doc
        :return: success message
        '''

        old_doc = IfapDoc.get(name=name_doc)
        if old_doc:
            old_doc.delete()
            obj.save()

        else:
            obj.save()

        return {'Message': '{} has been updated!'.formate(name_doc)}
