from digivlasna.core.documents.user import UserDoc
from digivlasna.core.constants import SOSMED_PLATFORMS
from digivlasna.core.exceptions import EntityValidationException
from elasticsearch.exceptions import NotFoundError
from datetime import datetime

class UserDocStore():
    
    def _validate_data(self, obj):
        if not isinstance(obj, (UserDoc)):
            raise EntityValidationException("Object must instance of UserDoc")
        if obj.platform not in SOSMED_PLATFORMS:
            raise EntityValidationException("platform must be set")
        obj.meta.id = UserDoc.get_meta_id(obj.platform, obj.id)
        return obj
    
    def createOrUpdate(self, obj):
        """
        create or update UserDoc
        Args: 
            UserDoc
        Returns:
            UserDoc: return value
        Raises:
            code.exceptions.EntityValidationException : if UserDoc attribute not meet rules
        """
        obj = self._validate_data(obj)
        try:
            oldObj = UserDoc.get(id = obj.meta.id)
            oldObj.crawling_at = datetime.now()
            return oldObj
        except NotFoundError as identifier:
            obj.save()
            return obj
    def get_by_meta_id(self, meta_id):
        """
        get UserDoc by meta_id
        
        Args:
            meta_id(int)
        Returns:
            UserDoc created userdoc
        Raises:
            core.exceptions.EntityValidationException 
        """
        ids = meta_id.split('-')
        if ids[0] not in SOSMED_PLATFORMS:
            raise EntityValidationException("Wrong meta id , meta id forma <platform>-<id>")
        try:
            return UserDoc.get(id=meta_id)
        except NotFoundError as ex:
            return None
    def delete_by_meta_id(self , meta_id):
        doc = self.get_by_meta_id(meta_id)
        if doc:
            doc.delete()
        return doc
    
    def update_location_by_meta_id(self, meta_id, location, city, country, country_code, lat, lon):
        doc = self.get_by_meta_id(meta_id)
        if doc:
            doc.country = country
            if lat:
                doc.centroid={'lat':lat, 'lon':lon}
            
            doc.country_code = country_code
            doc.save()
    


