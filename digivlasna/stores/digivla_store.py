from . import ElasticStoreBase
from digivlasna.core.documents.digivla import SosmedTopicDoc, TwitterAccountDoc, SosmedKeywordDoc
from digivlasna.core.documents.digivla import ClientDoc
from digivlasna.core.utils import el
from elasticsearch_dsl import Q
from elasticsearch.exceptions import NotFoundError


class ClientDocStore(ElasticStoreBase):

    def get_by_client_code(self, client_code):
        s = ClientDoc.search()
        s = s.filter('term',client_code=client_code)
        return s.execute()
    def delete_by_client_id(self, client_id):
        s = ClientDoc.search()
        s = s.filter('term', id=client_id)
        resp = s.execute()
        if not el.el_is_exists(resp):
            return
            # raise Exception("client not found")
        for item in resp:
            item.delete()
    def list_by_status(self, status, offset, count):
        s = ClientDoc.search()
        s = s.filter('term', status=status)
        start = offset * count
        end =  start + count
        s = s[start:end]
       

        return s.execute()
    def delete(self, uuid):
        doc = ClientDoc.get(uuid)
        doc.delete()
        
    def create(self, obj):
        if isinstance(obj,(ClientDoc)):
            obj.meta.id = obj.uuid
            obj.save()
            return obj
        else:
            raise Exception("object to save must instance of ClientDoc")
    def update(self, obj):
        if obj.uuid is None:
            raise Exception("no uuid found")
        oldObj = ClientDoc.get(id=obj.uuid)
        if oldObj is None:
            raise Exception("data not found")
        oldObj.name = obj.name
        oldObj.client_code = obj.client_code
        oldObj.icon = obj.icon
        oldObj.address = obj.address
        oldObj.phone = obj.phone
        oldObj.contact_name = obj.contact_name
        oldObj.email = obj.email
        oldObj.join_date = obj.join_date
        oldObj.expired_at = obj.expired_at
        oldObj.created_by = obj.created_by
        oldObj.status = obj.status
        
        oldObj.save()
        return oldObj

    def get(self, uuid):
        return ClientDoc.get(id=uuid)
        
    def createOrUpdate(self, obj):
        if not isinstance(obj, (ClientDoc)):
            raise Exception("obj is not instance of ClientDoc")
        if obj.id:
            return self.create(obj)
        else:
            return self.update(obj)
    def list(self, offset, count):
        s = ClientDoc.search()
        start = offset * count
        end =  start + count
        s = s[start:end]
        res = s.execute(ignore_cache=True)
        return res

class SosmedKeywordDocStore(ElasticStoreBase):
    def get(self, uuid):
        return SosmedKeywordDoc.get(id = uuid)
    def create(self, obj):
        if isinstance(obj, (SosmedKeywordDoc)):
            obj.meta.id = obj.uuid
            obj.save()
            return obj
        else:
            raise Exception("objet to save must instance of SosmedKeywordDoc")

    def update(self, obj):
        if obj.uuid is None:
            raise Exception("no uuid found")
        oldObj = SosmedKeywordDoc.get(id=obj.uuid)
        if oldObj is None:
            raise Exception("data not found")
        oldObj.keyword = obj.keyword
        oldObj.created_at = obj.created_at
        oldObj.client_count = obj.client_count
        oldObj.save()
        return oldObj

    def createOrUpdate(self, obj):
        if not isinstance(obj, (SosmedKeywordDoc)):
            raise Exception("obj is not instance of SosmedKeywordDoc")
        if obj.id:
            return self.create(obj)
        else:
            return self.update(obj)

    def delete(self, uuid):
        doc = SosmedKeywordDoc.get(uuid)
        doc.delete()
    def list(self, offset, count):
        s = SosmedKeywordDoc.search()
        start = offset * count
        end =  start + count
        s = s[start:end]
        res = s.execute()
        return res




class SosmedTopicDocStore(ElasticStoreBase):
    def get(self, uuid):
        try:
            return SosmedTopicDoc.get(id=uuid)
        except NotFoundError as ex:
            print(ex)
            return None
        
    
    def create(self,obj):
        if isinstance(obj,(SosmedTopicDoc)):
            obj.meta.id = obj.uuid
            
            obj.save()
            return obj
        else:
            raise Exception("object to save must instance of SosmedTopicDoc")
    
    def update(self, obj):
        if obj.uuid is None:
            raise Exception("no uuid found")
        oldObj = SosmedTopicDoc.get(id=obj.uuid)
        if oldObj is None:
            raise Exception("data not found")
        oldObj.topic = obj.topic
        oldObj.keyword = obj.keyword
        oldObj.client_id = obj.client_id
        oldObj.created_at = obj.created_at
        oldObj.save()
        return oldObj
    # def client_keywords(self, client_id, offset, count):
    #     s = SosmedTopicDoc.search()
    #     s = s.filter('client_id',client_id)
    #     s = s[offset: count]
    #     res = s.execute
    #     return res

    def createOrUpdate(self, obj):
        if not isinstance(obj, (SosmedTopicDoc)):
            raise Exception("obj is not instance of SosmedTopicDoc")
        if obj.id:
            return self.create(obj)
        else:
            return self.update(obj)

    def list(self, offset, count):
        s = SosmedTopicDoc.search()
        start = offset * count
        end =  start + count
        s = s[start:end]
        res = s.execute()
        return res

    def client_topics(self, client_id, offset, count):
        s = SosmedTopicDoc.search()
        s = s.filter('term', client_id = client_id)
        s = s[offset:count]
        res = s.execute()
        return res
    
    

    def delete(self, uuid):
        doc = SosmedTopicDoc.get(uuid)
        doc.delete()
    def delete_with_client_id(self, topic_uuid, client_id):
        try:
            docTopic = self.get(topic_uuid)
            if int(docTopic.client_id) == int(client_id) :
                docTopic.delete()

        except NotFoundError as ex:
            print(str(ex))
        except Exception as ex:
            print(str(ex))
        
        
        

