from . import ElasticStoreBase
from digivlasna.core.documents.twitter import StatusDoc


class TwitterStore(ElasticStoreBase):
    @staticmethod
    def migrate():
        StatusDoc.init()
        print("migrate StatusDoc done")
    
    @staticmethod
    def get_mapping():
        return StatusDoc._index.get_mapping()
    
    def save(self, obj):
        if isinstance(obj, (StatusDoc)):
            obj.save()
            return obj
        else:
           raise Exception("object to save must instance of StatusDoc")




