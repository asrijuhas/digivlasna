from digivlasna.core.documents.post import PostDoc, WordCount
from digivlasna.core.constants import SOSMED_PLATFORMS
from digivlasna.core.utils.word_count import get_words_count
from elasticsearch.exceptions import NotFoundError
from digivlasna.core.exceptions import EntityValidationException
from datetime import datetime

class PostDocStore():
    def _validated_data(self, obj):
        if not isinstance(obj,(PostDoc)):
            raise Exception("Object to save must instance of PostDoc")
        if obj.platform not in SOSMED_PLATFORMS:
            raise Exception("platform must be set")
        if obj.post_type not in ["post", "share","quote","reply"]:
            raise Exception( "post_type must be set")
        obj.meta.id = "%s-%s" %(obj.platform, obj.id)
        return obj

    
    def create(self, obj):
        obj = self._validated_data(obj)
        obj.save()
        return obj
        
    def update(self, obj):
        obj = self._validated_data(obj)
        if obj.id is None:
            raise Exception("no id set")
        oldObj = PostDoc.get(id=obj.id)
        if oldObj is None:
            raise Exception("data not found")
        oldObj.text = obj.text
        # oldObj.lang = obj.lang
        # oldObj.created_at = obj.created_at
        oldObj.reshared_count = obj.reshared_count
        # oldObj.in_reply_to_screen_name = obj.in_reply_to_screen_name
        # oldObj.in_reply_to_post_id = obj.in_reply_to_post_id
        # oldObj.in_reply_to_user_id = obj.in_reply_to_user_id
        # oldObj.reshared_post = obj.reshared_post
        # oldObj.hashtags = obj.hashtags
        # oldObj.user = obj.user
        # oldObj.user_mentions = obj.user_mentions
        oldObj.likes_count = obj.likes_count
        # oldObj.quoted_post = obj.quoted_post
        oldObj.urls = obj.urls
        # oldObj.location = obj.location
        # oldObj.country_code = obj.country_code
        # oldObj.coordinates = obj.coordinates
        oldObj.sentiment = obj.sentiment
        # oldObj.platform = obj.platform
        # oldObj.post_type = obj.post_type
        oldObj.digivla_keywords = obj.digivla_keywords
        # oldObj.crawled_created_at = obj.crawled_created_at
        oldObj.crawled_updated_at = obj.crawled_updated_at
        
        oldObj.save()
        return oldObj

    def get(self, id):
        ids = id.split('-')
        if ids[0] not in SOSMED_PLATFORMS:
            raise Exception("wrong id")

        return PostDoc.get(id=id)
        
    def createOrUpdate(self, obj):
        obj = self._validated_data(obj)
        print("============= ID ****>>>>>>>>> %s" %obj.id)
        
        try:
            oldObj = PostDoc.get(id=obj.meta.id)
            oldObj.reshared_count = obj.reshared_count
            oldObj.likes_count = obj.likes_count
            oldObj.comment_count = obj.comment_count
            oldObj.engagement = obj.engagement
            oldObj.post_reach = obj.post_reach
            old_keywords = oldObj.digivla_keywords
            cur_keywords = obj.digivla_keywords
            new_keywords = []    
            if type(cur_keywords) == str:
                cur_keywords = [cur_keywords]

            if type(old_keywords) == str:
                old_keywords = [old_keywords]
            new_keywords = [k for k in cur_keywords if k not in old_keywords]
            old_keywords.extend(new_keywords)
            
            oldObj.digivla_keywords = old_keywords       
            oldObj.crawled_updated_at= datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            oldObj.post_url = obj.post_url
            oldObj.save()
            return oldObj

        except NotFoundError as ex:
            obj.crawled_created_at = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            obj.crawled_updated_at = obj.crawled_created_at
            word_counts = get_words_count(obj.text)
            if type(obj.digivla_keywords ) == str:
                obj.digivla_keywords = [obj.digivla_keywords]
            for key, val in word_counts.items():
                oword_count = WordCount()
                oword_count.word = key
                oword_count.count = val
                obj.word_counts.append(oword_count)
            obj.save()
            return obj

    def createOrUpdateWithChangeStatus(self, obj):
        obj = self._validated_data(obj)
        print("============= ID ****>>>>>>>>> %s" %obj.id)
        is_changed=False
        
        try:
            oldObj = PostDoc.get(id=obj.meta.id)
            
            if oldObj.reshared_count != obj.reshared_count:
                oldObj.reshared_count = obj.reshared_count
                is_changed = True
            if oldObj.likes_count != obj.likes_count:
                oldObj.likes_count = obj.likes_count
                is_changed = True
            if oldObj.comment_count != obj.comment_count:
                oldObj.comment_count = obj.comment_count
                is_changed = True
            if oldObj.engagement != obj.engagement:
                oldObj.engagement = obj.engagement
                is_changed = True
            if oldObj.post_reach != obj.post_reach:
                oldObj.post_reach = obj.post_reach
                is_changed = True
            oldObj.created_at = obj.created_at
            oldObj.post_url = obj.post_url
            
            old_keywords = oldObj.digivla_keywords
                

            cur_keywords = obj.digivla_keywords
            new_keywords = []    
            if type(cur_keywords) == str:
                cur_keywords = [cur_keywords]

            if type(old_keywords) == str:
                old_keywords = [old_keywords]
            new_keywords = [k for k in cur_keywords if k not in old_keywords]
            old_keywords.extend(new_keywords)
            if len(new_keywords) > 0:
                is_changed = True
            
            oldObj.digivla_keywords = old_keywords       
            oldObj.crawled_updated_at= datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            # if is_changed:
            #     oldObj.save()
            oldObj.save()
            return oldObj, is_changed

        except NotFoundError as ex:
            is_changed = True
            obj.crawled_created_at = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            obj.crawled_updated_at = obj.crawled_created_at
            word_counts = get_words_count(obj.text)
            if type(obj.digivla_keywords ) == str:
                obj.digivla_keywords = [obj.digivla_keywords]
            for key, val in word_counts.items():
                oword_count = WordCount()
                oword_count.word = key
                oword_count.count = val
                obj.word_counts.append(oword_count)
            obj.save()
            return obj, is_changed


    def list(self, offset, count, platform=None,sdate=None, edate=None):
        s = PostDoc.search()
        if sdate and edate:
            s = s.filter('range',created_at={'from':sdate, 'to':edate})
        if platform:
            s = s.filter('term',platform=platform)
        # elif sdate:
        #     s = s.
        # elif edate:
        #     s =s.
        
        s = s[offset:count]
        res = s.execute(ignore_cache=True)
        return res

    def get_by_meta_id(self, meta_id):
        """
        get PlaceDoc by meta_id
        
        Args:
            meta_id(int)
        Returns:
            PlaceDoc created PlaceDoc
        Raises:
            core.exceptions.EntityValidationException 
        """
        ids = meta_id.split('-')
        
        if ids[0] not in SOSMED_PLATFORMS:
            raise EntityValidationException("Wrong meta id , meta id forma <platform>-<id>")
        try:
            return PostDoc.get(id=meta_id)
        except NotFoundError as ex:
            print(str(ex))
            return None
            
    def delete_by_meta_id(self, meta_id):
        doc = self.get_by_meta_id(meta_id)
        if doc:
            doc.delete()
        return doc
    
    def update_sentiment_by_metadata(self, meta_id, sentiment, sentiment_index):
        doc = self.get_by_meta_id(meta_id)
        if doc:
            doc.sentiment = sentiment
            doc.sentiment_index=sentiment_index
            doc.save()
        return doc