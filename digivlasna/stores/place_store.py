from digivlasna.core.documents.place import PlaceDoc
from digivlasna.core.constants import SOSMED_PLATFORMS
from elasticsearch.exceptions import NotFoundError
from datetime import datetime
from digivlasna.core.exceptions import EntityValidationException
class PlaceDocStore():
    
    def _validate_data(self, obj):
        if not isinstance(obj, (PlaceDoc)):
            raise EntityValidationException("Object must instance of PlaceDoc")
        if obj.platform not in SOSMED_PLATFORMS:
            raise EntityValidationException("platform must be set")
        obj.meta.id = PlaceDoc.get_meta_id(obj.platform, obj.id)
        return obj
    
    def createOrUpdate(self, obj):
        """
        create or update PlaceDoc
        Args: 
            PlaceDoc
        Returns:
            PlaceDoc: return value
        Raises:
            code.exceptions.EntityValidationException : if PlaceDoc attribute not meet rules
        """
        obj = self._validate_data(obj)
        try:
            oldObj = PlaceDoc.get(id = obj.meta.id)
            oldObj.crawling_at = datetime.now()
            return oldObj
        except NotFoundError as identifier:
            obj.save()
            return obj
    def get_by_meta_id(self, meta_id):
        """
        get PlaceDoc by meta_id
        
        Args:
            meta_id(int)
        Returns:
            PlaceDoc created PlaceDoc
        Raises:
            core.exceptions.EntityValidationException 
        """
        ids = meta_id.split('-')
        print("store ---- meta_id")
        print(ids)
        print(meta_id)
        if ids[0] not in SOSMED_PLATFORMS:
            raise EntityValidationException("Wrong meta id , meta id forma <platform>-<id>")
        try:
            return PlaceDoc.get(id=meta_id)
        except NotFoundError as ex:
            print(str(ex))
            return None
        
    def delete_by_meta_id(self, meta_id):
        doc = self.get_by_meta_id(meta_id)
        if doc:
            doc.delete()
        return doc

    


