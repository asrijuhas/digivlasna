from elasticsearch_dsl import connections, Document

class ElasticStoreBase(object):
    @staticmethod
    def create_connections(**kwargs):
        tmp_hosts = kwargs.get('hosts', None)
        http_auth = kwargs.get('http_auth',None)
        hosts=None
        
        
        if isinstance(tmp_hosts,(str)) :
            if ',' in tmp_hosts:
                hosts=tmp_hosts.split(',')
            else:
                hosts=[tmp_hosts]
        elif isinstance(tmp_hosts, (list,tuple)):
            hosts = tmp_hosts
        else:
            raise Exception("hosts must spesified, or wrong hosts value")

        if isinstance(http_auth, (str)):
            if ":" in http_auth:
                http_auth=(http_auth)
        
        if http_auth:
            connections.create_connection(hosts=hosts, http_auth=http_auth)

        else:
            connections.create_connection(hosts=hosts)
    # def __init__(self, obj):
    #     if isinstance(obj, (Document)):
    #         self.obj = obj
    #     else:
    #         raise Exception("obj must instance of elastic_dsl.Document")
    
    




