from py2neo.ogm import GraphObject, Property, RelatedFrom, RelatedTo
import neotime
from py2neo import Graph
from digivlasna.core.utils.time import parse_twitter_datetime_to_utc
import neotime

class Neo4jSettings(object):
    def __init__(self, url, user, password):
        self.url = url
        self.user = user
        self.password = password
    
    def get_graph(self):
        graph = Graph(url=self.url, user = self.user, password=self.password)
        return graph

class Country(GraphObject):
    __primarykey__ = "code"
    code = Property()
    name = Property()

class Location(GraphObject):
    __primarykey__ = "name"
    name = Property()
    parent_location = RelatedTo("Location")

class HashTag(GraphObject):
    __primarykey__ = "text"
    text = Property()

class Keyword(GraphObject):
    __primarykey__ = "text"
    text = Property()

class Topic(GraphObject):
    __primarykey__ = "topic_id"
    topic_id = Property()
    text = Property()
    topic_keyword = RelatedTo(Keyword)


class Person(GraphObject):
    __primarykey__ = "person_id"
    person_id = Property()
    screen_name = Property()
    name = Property()
    created_at = Property()
    followers_count = Property()
    likes_count = Property()
    friends_count = Property()
    post_count = Property()
    knows = RelatedTo("Person")



class Post(GraphObject):
    __primarykey__ = "post_id"
    post_id = Property()
    platform = Property()
    text = Property()
    created_at = Property()
    reshared_count = Property()
    comment_count = Property()
    likes_count = Property()
    view_count = Property() 
    post_type = Property()
    hashtags = RelatedTo(HashTag)
    has_keywords = RelatedTo(Keyword)
    posted_by = RelatedTo(Person)
    # reshared_by = RelatedTo(Person)
    
    mentions = RelatedTo(Person)
    in_country = RelatedTo(Country)
    in_location = RelatedTo(Location)
    shared_post = RelatedTo("Post")
    commented_post = RelatedTo("Post")
    
    @staticmethod
    def create_graph_connection(url, user, password):
        neoSetting = Neo4jSettings(url, user, password)
        return neoSetting.get_graph()
    
    @staticmethod
    def saveOrUpdateFromPostDoc(graph:Graph, data):
        post_type = data['post_type']
        platform = data['platform']
        post_id = data['id']
        text = data['text']
        created_at = data['created_at']
        if type(created_at) == str:
            created_at = parse_twitter_datetime_to_utc(data['created_at']).timestamp()    
        else:
            created_at = created_at.timestamp()
        created_at = neotime.DateTime.from_timestamp(created_at)
        
        reshared_count = data['reshared_count'] if 'reshared_count' in data else 0
        comment_count = data['comment_count'] if 'comment_count' in data else 0
        likes_count = data['likes_count'] if 'likes_count' in data else 0
        view_count = data['view_count'] if 'view_count' in data else 0
        hashtags=[]
        if 'hashtags'  in data:
            for item in data['hashtags']:
                hashtag = HashTag()
                hashtag.text = item
                hashtags.append(hashtag)
                graph.merge(hashtag)
        
        userMentions = []
        if 'user_mentions' in data:
            for item in data['user_mentions'] :
                person = Person()
                person.person_id = item['id']
                person.name = item['name']
                person.screen_name = item['screen_name']
                graph.merge(person)
                userMentions.append(person)
            
        #TODO location from data['location']
        
        # keywords =
        # for item in data['digivla_keywords']:
        #     keyword = Keyword()
        #     keyword.text = item.replace("#",'')
        #     graph.merge(keyword)
        #     keywords.append(keyword)
        
            
        
        postedUser = Person()
        postedUser.person_id = data['user']['id']
        postedUser.screen_name =  data['user']['screen_name'] if 'screen_name' in data['user'] else "anonym"
        postedUser.name = data['user']['name'] if 'name' in data['user'] else postedUser.screen_name
        if postedUser.screen_name == 'anonym':
            print("NO SCREEN_NAME FOUND set to ANONYM")
        graph.merge(postedUser)

        post = Post()
        post.post_id = post_id
        post.platform = platform
        post.text = text
        post.created_at = created_at
        post.reshared_count = reshared_count
        post.comment_count = comment_count
        post.likes_count = likes_count
        post.view_count = view_count
        post.post_type = post_type
        for item in hashtags:
            post.hashtags.add(item)
        # for item in keywords:
        if type(data['digivla_keywords']) == list:
            for k in data['digivla_keywords']:
                keyword = Keyword()
                keyword.text = k
                graph.merge(keyword)
                post.has_keywords.add(keyword)
        elif type(data['digivla_keywords']) == str:
            keyword = Keyword()
            keyword.text = data['digivla_keywords']
            graph.merge(keyword)
            post.has_keywords.add(keyword)

        post.posted_by.add(postedUser)
        
        for person in userMentions:
            post.mentions.add(person)
        
        if post_type == "quote":
            orignalPostUser = Person()
            orignalPostUser.person_id = data['quoted_post']['user_id']
            orignalPostUser.screen_name = data['quoted_post']['screen_name']
            graph.merge(orignalPostUser)
            originalPost = Post()
            originalPost.post_id = data['quoted_post']['id']
            originalPost.posted_by.add(orignalPostUser)
            
            graph.merge(originalPost)
            
            post.shared_post.add(originalPost)
            
        elif post_type == "reply":
            if 'in_reply_to_user_id' in data:
                originalPostUser = Person()
                originalPostUser.person_id = data['in_reply_to_user_id']
                originalPostUser.screen_name = data['in_reply_to_screen_name']
                graph.merge(originalPostUser)

            originalPost = Post()
            originalPost.post_id = data['in_reply_to_post_id']
            if 'in_reply_to_user_id' in data:
                originalPost.posted_by.add(originalPostUser)
            
            graph.merge(originalPost)
            
            post.commented_post.add(originalPost)
            
        
        
        print("save post to neo4j")
        graph.merge(post)
        
        





