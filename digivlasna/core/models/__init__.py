import binascii
import datetime
import os
import uuid

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import UserManager

from digivlasna.core.constants import COMPANY_CHOICES, SOSMED_PLATFORMS_CHOICES
from .managers import SoftDeletetionManager, ValidUserManager
from .identity import Identity
from digivlasna.core import exceptions
from digivlasna.core import constants


class BaseModel(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True,db_index=True)
    created_at = models.DateTimeField(default=timezone.now)
    # created_by = models.ForeignKey("digivla.User", on_delete=models.DO_NOTHING, related_name="%(app_label)s_%(class)s_related")
    created_by = models.CharField(max_length=32)
    updated_at = models.DateTimeField(default=timezone.now)
    updated_by = models.CharField(max_length=32)
    # update_by = models.ForeignKey("digivla.User", on_delete=models.DO_NOTHING, related_name="%(app_label)s_%(class)s_related")

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.uuid:
            self.created_at = timezone.now()

        self.updated_at = timezone.now()
        return super().save(*args, **kwargs)

class SoftDeletetionModel(models.Model):
    deleted_at = models.DateTimeField(blank=True,  null=True)
    objects = SoftDeletetionManager()
    all_objects = SoftDeletetionManager(alive_only=False)
    # objects =
    class Meta:
        abstract=True

    def delete(self):
        self.deleted_at = timezone.now()
        self.save()
    def hard_delete(self):
        super().delete()

class Client(BaseModel, SoftDeletetionModel):
    name = models.CharField(max_length=32, db_index=True, unique=True)
    client_code = models.CharField(max_length=32, db_index=True)
    icon = models.CharField(max_length=255, null=True, blank=True)
    address = models.CharField(max_length=1024)
    phone = models.CharField(max_length=15)
    contact_name = models.CharField(max_length=25)
    email = models.EmailField(max_length=50,)
    join_date = models.DateField()
    expired_at = models.DateField()
    status = models.CharField(choices=COMPANY_CHOICES, max_length=16)
    deleted_at = models.DateTimeField(null=True, blank=True)

    def delete(self):
        self.deleted_at = timezone.now()
        self.save()
    # def save(self, *args, **kwargs):
    #     super().save

    class Meta:
        db_table = "clients"
        app_label = 'digivla'

STATUS_USERS = (('ACTIVE','ACTIVE'),('SUSPEND','SUSPEND'))
class User(BaseModel, AbstractBaseUser):
    username = models.CharField(max_length=32, unique=True)
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)
    email = models.EmailField(max_length=32)
    phone = models.CharField(max_length=15, default='-')
    is_staff = models.BooleanField(default=False)
    status = models.CharField(max_length=20, choices=STATUS_USERS)
    client = models.ForeignKey(Client, on_delete=models.DO_NOTHING, null=True, blank=True)
    is_superuser = models.BooleanField(default=False)
    expired_at = models.DateField(null=True, blank=True)
    objects = UserManager()
    valid_users = ValidUserManager()
    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["email", "first_name"]

    def has_perm(self, perm, obj=None):
        return self.is_superuser
    def has_module_perms(self, app_label):
        return self.is_superuser
    def get_identity(self):
        client = self.client
        userRoles = UserRoles.objects.filter(user__id=self.pk)
        roles =[]
        is_anlyst = False
        for item in userRoles:
            if item.role.name == constants.ROLE_NAME_ANALYST:
                is_anlyst = True
            roles.append(item.role.name)
        project_clients = []
        if is_anlyst:
            projects = Project.objects.filter(user=self)
            for project in projects:
                item_project ={'client_uuid': project.client.uuid.hex, 'client_name':project.client.name}
                project_clients.append(item_project)
        else:
            activeClients = Client.objects.filter(status='active').all()
            for cl in activeClients:
                item_project ={'client_uuid':cl.uuid.hex, 'client_name':cl.name}
                project_clients.append(item_project)

        default_client_uuid = client.uuid.hex if  client else None
        default_client_name = client.name if client else None

        # if not default_client_uuid and self.is_superuser:
        #     client_default = Client.objects.filter(status='active').first()
        #     if client_default:
        #         default_client_uuid = client_default.uuid
        #         default_client_name = client_default.name
        if not default_client_uuid:
            if len(project_clients) > 0:
                default_client_uuid = project_clients[0]['client_uuid']
                default_client_name = project_clients[0]['client_name']


        data ={
            'username':self.username,
            'first_name':self.first_name,
            'last_name':self.last_name,
            'email': self.email,
            'uuid': self.uuid.hex,
            'is_staff': self.is_staff,
            'is_superuser': self.is_superuser,
            'roles': roles,
            'client_id': client.pk if client else None,
            'client_uuid': client.uuid.hex if client else default_client_uuid,
            'client_name': client.name if client else default_client_name,
            'client_code': client.client_code if client else None,
            'client_address': client.address if client else None,
            'client_phone':client.phone if client else None,
            'client_contact_name':client.contact_name if client else None,
            'client_email': client.email if client else None,
            'client_join_date':client.join_date if client else None,
            'client_expired_at':client.expired_at if client else None,
            'client_status': client.status if client else None,
            'project_clients':project_clients,
        }
        identity = Identity(**data)
        return identity
    @property
    def roles(self):
        roles = UserRoles.objects.filter(user=self)
        role_array=[]
        for r in roles:
            role_array.append(r.role.name)
        return role_array

    class Meta:
        db_table = "users"
        app_label = 'digivla'


class Role(BaseModel):
    """
    Roles terdiri dari:
    1. superadmin
    2. admin
    3. analyst
    4. client_user
    5. client_analyst
    """

    name = models.CharField(max_length=16, unique=True)
    description = models.CharField(max_length=512)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "roles"
        app_label = 'digivla'


class UserRoles(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_index=True)
    role = models.ForeignKey(Role, on_delete=models.DO_NOTHING, db_index=True)

    def __str__(self):
        return "%s [%s] "%(self.user.username, self.role.name)
    class Meta:
        db_table = "user_roles"
        app_label = 'digivla'
        unique_together =('user','role')

class UserToken(BaseModel):
    token = models.CharField(max_length=64, unique=True, db_index=True)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_index=True)
    expired_at = models.DateTimeField(
        default=timezone.now() + datetime.timedelta(minutes=60)
    )

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = binascii.hexlify(os.urandom(20)).decode()
        super().save(*args, **kwargs)

    @staticmethod
    def get_or_create_user_token(user):
        if not user:
            return None
        userToken = UserToken.objects.filter(user=user).first()
        if userToken:
            if userToken.expired_at < timezone.now():
                userToken.delete()
                userToken = None

        if not userToken:
            userToken = UserToken()
            userToken.user =user
            userToken.save()

        return userToken
    def __str__(self):
        return "%s (%s)" %(self.user.username, self.token)
    @staticmethod
    def get_user_by_token(token):
        if not token:
            raise Exception("please provide token")
        userToken = UserToken.objects.filter(token=token).first()
        if userToken is None:
            raise exceptions.TokenNotFoundException("token not exist")
        return userToken.user

    @staticmethod
    def refresh_token(token):
        if not token:
            raise Exception("please provide token")
        userToken = UserToken.objects.filter(token=token).first()
        if userToken is None :
            raise Exception("token not found")
        userToken.expired_at = timezone.now() + datetime.timedelta(minutes=60)
        userToken.save()

    @staticmethod
    def clear_token(token):
        if token:
            userToken = UserToken.objects.filter(token=token).first()
            if userToken:
                userToken.delete()


    class Meta:
        db_table = "user_tokens"
        app_label = 'digivla'

class Project(BaseModel):
    name = models.CharField(max_length=64)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    client = models.ForeignKey(Client,  on_delete=models.CASCADE)

    class Meta:
        db_table = 'projects'
        app_label= 'digivla'
        unique_together =('user','client')
    # def save(self, *args, **kwargs):
    #     user_is_analyst = UserRoles.objects.filter(user=self.user, role='analyst' ).exists()
    #     if not user_is_analyst:
    #         raise Exception("user must in analyst role")
    #     super().save(*args, **kwargs)


class SosmedKeyword(BaseModel):
    keyword = models.CharField(max_length=64, unique=True, db_index=True)
    class Meta:
        db_table = 'sosmed_keywords'
        app_label = 'digivla'
    @property
    def client_count(self):
        return ClientSosmedKeyword.objects.filter(keyword__keyword__exact = self.keyword).count()

class ClientSosmedKeyword(BaseModel):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    keyword = models.ForeignKey(SosmedKeyword, on_delete=models.CASCADE)
    exclude_text = models.CharField(max_length=1024, blank=True, null=True)
    class Meta:
        db_table = 'client_keywords'
        app_label = 'digivla'


class Influencer(BaseModel):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1024)


    class Meta:
        db_table ='influencer'
        app_label = 'digivla'

class InfluencerAccount(BaseModel):
    user_id = models.CharField(max_length=25)
    platform = models.CharField(choices=SOSMED_PLATFORMS_CHOICES, max_length=20)
    influencer = models.ForeignKey(Influencer, on_delete=models.CASCADE)
    class Meta:
        db_table = 'influencer_account'
        app_label = 'digivla'

class ClientInfluencer(BaseModel):
    client = models.ForeignKey(Client   , on_delete=models.CASCADE)
    influencer = models.ForeignKey(Influencer, on_delete=models.CASCADE)
    class Meta:
        db_table = 'client_influencer'
        app_label = 'digivla'




class SosmedTopic(BaseModel):
    name = models.CharField(max_length=64)
    client = models.ForeignKey(Client,on_delete=models.CASCADE)
    keywords = models.ManyToManyField(SosmedKeyword,related_name='sosmedtopic')
    class Meta:
        db_table = 'sosmed_topics'
        app_label = 'digivla'

    # def save(self, *args, **kwargs):
    #     for keword in self.keywords:
    #         if not s.objects.filter(client = client, keyword__keyword=keword.keyword).exists():


class Chart(BaseModel):
    name = models.CharField(max_length=100, unique=True, db_index=True)
    description = models.CharField(max_length=500, default='-')
    class Meta:
        db_table='list_chart'
        app_label='digivla'


class ClientChart(BaseModel):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    chart = models.ForeignKey(Chart, on_delete=models.CASCADE)

    class Meta:
        db_table = 'client_chart'
        app_label = 'digivla'


class HiddenTopic(BaseModel):
    topic = models.CharField(max_length=100, db_index=True)
    description = models.CharField(max_length=500, default='-')
    type_key = models.CharField(max_length=100)

    class Meta:
        db_table = 'hidden_key'
        app_label = 'digivla'

class IfapDocName(BaseModel):
    doc_name = models.CharField(max_length=100, db_index=True)
    client = models.CharField(max_length=100)

    class Meta:
        db_table = 'ifap_docname'
        app_label = 'digivla'
