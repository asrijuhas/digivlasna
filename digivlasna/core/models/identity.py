class Identity():

    def __init__(self, **kwargs):

        self.params_default={
            'username':None,
            'first_name':None,
            'last_name':None,
            'email':None,
            'uuid':None,
            'is_staff':None,
            'is_superuser':None,
            'roles':None,
            'client_id':None,
            'client_uuid':None,
            'client_name':None,
            'client_code':None,
            'client_address':None,
            'client_phone':None,
            'client_contact_name':None,
            'client_email':None,
            'client_join_date':None,
            'client_expired_at':None,
            'client_status':None,
            'project_clients':None
        }
        for (param, default) in self.params_default.items():
            setattr(self, param, kwargs.get(param, default))
    
    def AsDict(self):
        data={}
        for (key, default) in self.params_default.items():
            data[key] = getattr(self, key, default)
        return data
