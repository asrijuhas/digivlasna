from django.db import models
from django.utils import timezone

#https://adriennedomingus.com/blog/soft-deletion-in-django

class SoftDeletionQuerySet(models.QuerySet):
    def delete(self):
        return super().update(deleted_at=timezone.now())

    def hard_delete(self):
        return super().delete()
    def alive(self):
        return self.filter(deleted_at = None)
    def dead(self):
        return self.exclude(deleted_at=None )
    
    


class SoftDeletetionManager(models.Manager):
    def __init__(self, *args, **kwargs):
        self.alive_only = kwargs.pop('alive_only',True)
        super().__init__(*args, **kwargs)
    
    def get_queryset(self):
        if self.alive_only:
            return SoftDeletionQuerySet(self.model).filter(deleted_at=None)
        return SoftDeletionQuerySet(self.model)

class ValidUserManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(expired_at__gt = timezone.now())

# class UserRoleAnalyst(models.QuerySet)