from datetime import date, datetime, time, timedelta
from django.utils import timezone
import pytz

TWITTER_DATE_TIME_FORMAT = '%Y-%m-%d %H:%M:%S'
APPLICATION_DATE_TIME_FORMAT = '%Y-%m-%d %H:%M:%S'
APPLICATION_DATE_FORMAT = '%Y-%m-%d'

def parse_twitter_datetime_to_utc(str_time:str) -> datetime:
    str_time=str_time.replace("T"," ")
    return datetime.strptime(str_time, TWITTER_DATE_TIME_FORMAT)


def timestamp_to_datetime( timestamp:int):
    return datetime.fromtimestamp(timestamp)

class ElasticSearchTimeFilterHelper(object):
    @staticmethod
    def get_today_range_time():
        delta = timedelta(hours=7)
        now = timezone.now().date()
        sdate = datetime.combine(now, time.min)
        edate = now - delta
        edate = datetime.combine(edate, time.max)


        return sdate, edate

    @staticmethod
    def normalize_sdate( sdate):

        sdate = "%s 00:00:00" %sdate
        sdate= datetime.strptime( sdate,'%Y-%m-%d %H:%M:%S')

        return sdate
    @staticmethod
    def normalize_edate( edate):
        edate = "%s 23:59:59" %edate

        edate = datetime.strptime( edate,'%Y-%m-%d %H:%M:%S')

        return edate

    @staticmethod
    def normalize_sdate_oneday( sdate):
        sdate = "%s 00:00:00" %sdate
        sdate= datetime.strptime( sdate,'%Y-%m-%d %H:%M:%S')
        return sdate
    @staticmethod
    def normalize_edate_oneday( edate):
        edate = "%s 23:59:59" %edate
        edate = datetime.strptime( edate,'%Y-%m-%d %H:%M:%S')
        return edate

    @staticmethod
    def get_time_range_mode(sdate, edate):
        '''
        return mode : hour | day | month | year

        '''
        timedelta = edate - sdate
        if timedelta.days <= 1:
            return 'hour'
        elif timedelta.days <= 1095:
            return 'day'
        elif timedelta.days <=  2555:
            return 'month'
        return 'year'

    @staticmethod
    def to_localtime(utctime):

        if 'T' not in utctime:
            raise Exception("utctime must in format Timezone ")
        fmt="%Y-%m-%d %H:%M:%S"
        utctime=utctime.replace('T', ' ')
        utctime = datetime.strptime(utctime, fmt)

        utc = utctime.replace(tzinfo=pytz.UTC)
        localtz = utc.astimezone(timezone.get_current_timezone())
        return localtz.strftime(fmt)