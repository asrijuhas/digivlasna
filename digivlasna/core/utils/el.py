#check apakah datanya exist
def el_is_exists(response):
    
    return response.hits.total.value > 0

# total dokumen yang cocok  dari query yang kita lakukan
def el_total_matched(response):
    return response.hits.total.value

#berapa lama waktu yang dibutuhkan untuk melakukan query dalam ms
def el_response_time_ms(response):
    return response.took

# score / nilai dari dokumen yang paling relevant
def el_max_score(response):
    return response.hits.max_score

# total shard
def el_shard_total(response):
    return response._shards.total

# shard yang sukses
def el_shard_succes(response):
    return response._shards.successful

#shard yang di skip
def el_shard_skipped(response):
    return response._shards.skipped
#check apakah timeout
def el_is_timeout(response):
    return response.timed_out

#shard yang failed
def el_shard_failed(response):
    return response._shards.failed

#get first doc
def el_get_first_doc(response):
    if el_is_exists(response):
        return response.hits[0]
    return None

#get documents only from response
def get_documents(response):
    return response.hits
def get_documents_dict(response):
    data = map(lambda doc: doc['_source'], response.to_dict()['hits']['hits'])
    return data
    # return response.to_dict()['hits']['hits']


