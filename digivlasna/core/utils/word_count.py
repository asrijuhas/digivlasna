from digivlasna.constants.words_dataset import excluded_wordcloud

def get_words_count(wordstring):
    wordstring = wordstring.lower()
    wordlist = wordstring.split()
    wordlist =[word for word in wordlist if word not in excluded_wordcloud]
    wordfreq = []
    for w in wordlist:
        wordfreq.append(wordlist.count(w))
    
    word_counts = dict(list(zip(wordlist, wordfreq))) # return dictionary {'it':2, 'dan':3}
    return word_counts
