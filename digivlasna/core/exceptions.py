class EntityNotFoundException(Exception):
    pass

class WrongRoleException(Exception):
    pass

class TokenNotFoundException(Exception):
    pass

class EntityValidationException(Exception):
    pass