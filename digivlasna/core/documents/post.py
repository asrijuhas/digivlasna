from elasticsearch_dsl import Document, Date, Nested, Boolean, analyzer, InnerDoc, Completion, Keyword ,Text,Integer, GeoShape, GeoPoint, MetaField

posting_analyzer = analyzer('posting_analyzer', tokenizer="standard", filter=["lowercase", "stop"])
class Media(InnerDoc):
    # id = Keyword()
    url = Keyword()
    type = Keyword() # image, video

class User(InnerDoc):
    id = Keyword()
    screen_name = Keyword()
    name = Keyword()
    created_at = Date()
    followers_count = Integer()
    likes_count = Integer()
    friends_count = Integer()
    post_count = Integer()
    geo_enabled = Boolean()
    location = Text() # ex: jakarta
    country = Keyword() # ex: indonesia
    country_code = Keyword() # ex: ID
    centroid = GeoPoint()
    account_status = Keyword() # official, verified dll
    account_type = Keyword() # tipe akun misal media, personal, dll

class UserMention(InnerDoc):
    id = Keyword()
    name = Keyword()
    screen_name = Keyword()

# ini adalah status yang di share atau di retweet tanpa menambahkan text apapun
class ReSharedPost(InnerDoc):
    id = Keyword()
    user_id = Keyword()
    screen_name = Keyword()
    platform = Keyword()
    # id = Keyword()
    # text = Text()
    # lang = Keyword()
    # urls = Keyword() #url yang tersematkan dalam post yang di retweet atau di repost
    # user = Nested(User)
    # user_mentions = Nested(UserMention)
    # created_at = Date()
    # media = Nested(Media)

# sama dengan SharedPost tapi QuotedPost adalah Post yang di retweet /share /repost dengan tambahan
# text oleh user
class QuotedPost(InnerDoc):
    id = Keyword()
    user_id = Keyword()
    screen_name = Keyword()
    platform = Keyword()
    # id = Keyword()
    # text = Text()
    # lang = Keyword()
    # created_at = Date()
    # retweet_count = Integer()
    # hashtags = Keyword() # array
    # user_mentions = Nested(UserMention)
    # likes_count = Integer()
    # media = Nested(Media) # media yang mungkin di sematkan di posting

class Place(InnerDoc):
    id = Keyword()
    centroid = GeoPoint()
    country = Keyword()
    country_code = Keyword()
    full_name = Text()
    name = Keyword()
    place_type = Keyword()
    url = Text()

class TextEntity(InnerDoc):
    mentionNumber = Integer()
    opinionIndex = Integer()
    name = Keyword()
    type = Keyword()

class TextAnalysis(InnerDoc):
    opinionIndex = Integer()
    inputLength = Integer()
    summarizedPositiveOpinionIndex=Integer()
    summarizedNegativeOpinionIndex=Integer()
    entities = Nested(TextEntity)

class WordCount(InnerDoc):
    word = Keyword()
    count = Integer()

class PostDoc(Document):
    id = Keyword()
    # text = Text(fielddata=True) # untuk youtube text diambil dari description
    text = Text(analyzer=posting_analyzer, fields={'keyword':Keyword()})
    post_url = Text() # url asli dari posting
    title = Text() # ini pada youtube ada title
    lang = Keyword()
    created_at = Date()
    reshared_count = Integer() # retweet
    in_reply_to_screen_name = Keyword() # username yang post nya di comm
    in_reply_to_post_id = Keyword() # post id yang text nya di comment
    in_reply_to_user_id = Keyword() # jika ke 3 attribute ini ada maka post bukan status melainkan comment terhadap post
    reshared_post = Nested(ReSharedPost)
    hashtags = Keyword()
    user = Nested(User)
    user_mentions = Nested(UserMention) #user yang di mention di dalam post
    likes_count = Integer()
    dislike_count = Integer() # biasanya youtube
    comment_count = Integer() # comment count
    view_count = Integer()
    quoted_post = Nested(QuotedPost)
    digivla_tags = Keyword()
    urls = Keyword() # array url yang mungkin di sematkan pada post
    location = Nested(Place)
    country_code = Keyword()
    centroid = GeoPoint()
    sentiment = Keyword()
    sentiment_index = Integer()
    post_reach = Integer()
    engagement = Integer()
    is_deleted = Boolean()
    deleted_for = Keyword() # array client
    media =Nested(Media)
    platform = Keyword() #facebook, instagram, tweeter, youteube
    post_type = Keyword() #post, share, quote, reply
    digivla_keywords = Keyword()
    crawled_created_at = Date()
    crawled_updated_at = Date()
    text_analysis=Nested(TextAnalysis) # neticle text analysis
    word_counts = Nested(WordCount)

    class Index:
        name = 'sosmed-posts'
    class Meta:
        dynamic = MetaField('strict')

class DistOfMentions(InnerDoc):
    negative = Integer()
    very_positive = Integer()
    positive = Integer()
    very_negative = Integer()
    summary = Integer()
    neutral = Integer()

class DataIfapDoc(InnerDoc):
    distribution_of_mentions = Nested(DistOfMentions)
    top_10_labels = Nested()
    top_5_most_positive_labels = Nested()
    top_5_most_negative_labels = Nested()
    top_5_strongest_positive_correlations = Nested()
    top_5_strongest_negative_correlations = Nested()
    custom_labels = Nested()
    top_3_longest_texts = Nested()
    top_3_most_positive_texts = Nested()
    top_3_most_negative_texts = Nested()

class IfapDoc(Document):
    id = Keyword()
    doc_name = Keyword()
    data = Nested(DataIfapDoc)
    client = Keyword()
    created_at = Date()
    updated_at = Date()

    class Index:
        name = 'ifap-docs'

    class Meta:
        dynamic = MetaField('strict')
