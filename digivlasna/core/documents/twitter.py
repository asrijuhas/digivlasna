from elasticsearch_dsl import Document, Date, Nested, Boolean, analyzer, InnerDoc, Completion, Keyword ,Text,Integer, GeoShape, MetaField

class Media(InnerDoc):
    url = Text()
    type = Keyword()
    

# class Trend(Document):


class Url(InnerDoc):
    url = Text()
    expanded_url = Text()

class User(InnerDoc):
    id = Keyword()
    description = Text()
    created_at = Date()
    favourites_count = Keyword()
    followers_count = Keyword()
    following = Boolean()
    friends_count = Keyword()
    geo_enabled = Boolean()
    listed_count = Keyword()
    location = Text()
    name = Keyword()
    statuses_count = Keyword()
    profile_image_url = Text()




    
class UserMention(InnerDoc):
    id = Keyword()
    name = Keyword()
    screen_name = Keyword()

class Place(InnerDoc):
    id = Keyword()
    bounding_box = GeoShape()
    country = Keyword()
    country_code = Keyword()
    full_name = Text()
    name = Keyword()
    place_type = Keyword()
    url = Text()


class RetweetedStatus(InnerDoc):
    id = Keyword()
    text = Text()
    lang = Keyword()
    source = Text()
    urls = Text()
    user = Nested(User)
    user_mentions = Nested(UserMention)
class QuotedStatus(InnerDoc):
    id = Keyword()
    text = Text()
    lang = Keyword()
    created_at = Date()
    current_user_retweet = Keyword()
    retweet_count = Keyword()
    retweeted = Boolean()
    # in_reply_to_screen_name = Keyword()
    # in_reply_to_status_id = Keyword()
    # in_reply_to_user_id = Keyword()
    retweeted_status = Nested(RetweetedStatus)
    hashtags = Keyword()
    user = Nested(User)
    user_mentions = Nested(UserMention)
    favorite_count = Keyword()
    favorited = Boolean()

class StatusDoc(Document):
    id = Keyword()
    text = Text()
    lang = Keyword()
    created_at = Date()
    current_user_retweet = Keyword()
    retweet_count = Keyword()
    retweeted = Boolean()
    in_reply_to_screen_name = Keyword()
    in_reply_to_status_id = Keyword()
    in_reply_to_user_id = Keyword()
    retweeted_status = Nested(RetweetedStatus)
    hashtags = Keyword()
    user = Nested(User)
    user_mentions = Nested(UserMention)
    favorite_count = Keyword() # ?? check lagi
    favorited = Boolean()
    quoted_status = Nested(QuotedStatus)
    quoted_status_id = Keyword()
    possibly_sensitive = Boolean()
    urls = Nested(Url)
    media = Nested(Media)
    place = Nested(Place)
    location = Text()
    coordinates = GeoShape()
    topic_ids = Keyword()
    source = Text()
    client_ids = Keyword()

    class Index:
        name = "twitter-post"
    
    class Meta:
        dynamic = MetaField('strict')


