from elasticsearch_dsl import Document, Keyword, Text, Date, MetaField

class DigivlaLog(Document):
    timestamp = Date() 
    event_dataset = Keyword() # contoh: apache.access
    event_original = Text() # contoh: 10.42.42.42 - - [07/Dec ...
    message = Text() # contoh: "GET /blog HTTP/1.1" 200 2571
    service_name = Keyword() # Your custom name for this service contoh: Company blog
    service_type = Keyword() # contoh: apache
    level = Keyword() # info, error, debug

    class Index:
        name = 'digivla-logs'
    class Meta:
        dynamic = MetaField('strict')
    