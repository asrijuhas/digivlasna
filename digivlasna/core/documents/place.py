from elasticsearch_dsl import Document, Date, Nested, Boolean, analyzer, InnerDoc, Completion, Keyword ,Text,Integer, GeoShape, MetaField, GeoPoint

class PlaceDoc(Document):
    id = Keyword()
    country_code = Keyword()
    platform = Keyword()
    country = Keyword()
    parent_id = Keyword()
    name = Text()
    centroid = GeoPoint()
    crawling_at = Date()

    @staticmethod
    def get_meta_id(platform, id):
        return "%s-%s" %(platform, id)

    class Index:
        name = 'sosmed-place'
    class Meta:
        dynamic = MetaField('strict')
