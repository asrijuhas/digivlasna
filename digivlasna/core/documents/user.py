from elasticsearch_dsl import Document, Date, Nested, Boolean, analyzer, InnerDoc, Completion, Keyword ,Text,Integer, GeoPoint, MetaField

class UserDoc(Document):
    id = Keyword()
    name = Keyword()
    platform = Keyword() # fb | tw | yt | ig |
    screen_name = Keyword()
    created_at = Date()
    followers_count = Integer()
    likes_count = Integer()
    friends_count = Integer()
    is_verified = Boolean()
    post_count = Integer()
    geo_enabled = Boolean()
    location = Text() # ex: jakarta
    country = Keyword() # ex: indonesia
    country_code = Keyword() # ex: ID
    centroid = GeoPoint()
    crawling_at = Date()
    
    @staticmethod
    def get_meta_id(platform, id):
        return "%s-%s" %(platform, id)
    class Index:
        name = 'sosmed-user'
    class Meta:
        dynamic = MetaField('strict')