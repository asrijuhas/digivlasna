from elasticsearch_dsl import Document, Integer, Nested, InnerDoc, Boolean, Keyword

class DashboardPreference(InnerDoc):
    w = Integer()
    h = Integer()
    y = Integer()
    x = Integer()
    i = Keyword() # type chart
    minW = Integer()
    maxW = Integer()
    minH = Integer()
    maxH = Integer()
    moved = Boolean()
    static = Boolean()
    isDraggable = Boolean()
    isResizable = Boolean()

class UserPreferenceDoc(Document):
    id = Keyword()
    dashboard_preferences = Nested(DashboardPreference)

    class Index:
        name = 'user-preferences'

