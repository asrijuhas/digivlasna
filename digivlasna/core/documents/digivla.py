from elasticsearch_dsl import Document, Date, Nested, Boolean, analyzer, InnerDoc, Completion, Keyword ,Text,Integer, GeoShape, MetaField

class TwitterAccountDoc(Document):
    id=Keyword()
    account_name =Keyword()
    consumer_key = Keyword()
    consumer_secret = Keyword()
    access_token = Keyword()
    access_token_secret = Keyword()

    class Index:
        name="twitter-accounts"



class SosmedKeywordDoc(Document):
    id = Keyword()
    uuid = Keyword()
    keyword = Keyword()
    last_crawled_at = Date()
    client_count = Integer()
    created_at = Date()
    created_by = Keyword()
    updated_by = Keyword()

    class Index:
        name="sosmed-keywords"


class SosmedTopicDoc(Document):
    id = Keyword()
    uuid = Keyword()
    topic = Keyword()
    keyword = Keyword()
    client_id = Keyword()
    created_at = Date()
    created_by = Keyword()
    updated_by = Keyword()

    class Index:
        name="sosmed-topics"

    def get_stats(self):
        return SosmedTopicDoc._index.stats()
    def is_index_exists(self):
        return SosmedTopicDoc._index.exists()
    
    def get_settings(self):
        return SosmedTopicDoc._index.get_settings()
    
    def save_or_update(self):
        pass

class ClientDoc(Document):
    id = Keyword()
    uuid = Keyword()
    name = Keyword()
    client_code = Keyword()
    icon = Text()
    address = Text()
    phone = Text()
    contact_name = Text()
    email = Keyword()
    joint_date = Date()
    expired_at = Date()
    created_by = Keyword()
    status = Keyword()
    deleted_at = Date()
    created_by = Keyword()
    updated_by = Keyword()

    class Index:
        name = "client-companies"
