COMPANY_CHOICES = (('active','active'),('trial','trial'),('suspend', 'suspend'))
ROLE_NAME_ANALYST = 'analyst'
ROLE_NAME_ADMIN = 'admin'
ROLE_NAME_CLIENT_USER = 'client_user'

SOSMED_PLATFORMS =['fb','ig','tw','yt','blog','forum']
SOSMED_PLATFORMS_CHOICES = (('fb','facebook'), ('tw', 'twitter'), ('ig', 'instagram'), ('yt', 'youtube'),('blog', 'blog'), ('forum', 'forum'))