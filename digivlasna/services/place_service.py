from digivlasna.core.documents.place import PlaceDoc
from digivlasna.stores.place_store import PlaceDocStore

class PlaceService():
    '''
    PlaceService : use to wrapping process CRUD PlaceDoc to PlaceDocStore
    '''
    def __init__(self):
        self.default_params = {
            'id':None,
            'country_code':None,
            'platform':None,
            'country':None,
            'parent_id':None,
            'name':None,
            'centroid':None,
            'crawling_at':None
        }
    def saveOrUpdatePlaceFromDict(self,**kwargs):
        """
        save or update place from dictionary
        Args: 
            **kwargs key value attribute for PlaceDoc defined on self.default_params
        Returns:
            PlaceDoc: return value 
        Raises:
            core.exceptions.EntityValidationException
        """
        place = PlaceDoc()
        for item, default  in self.default_params.items():
            setattr(place, item, kwargs.get(item, default))
        
        meta_id = PlaceDoc.get_meta_id(place.platform, place.id)
        existingPlace  = self.getPlaceByMetaId(meta_id)
        if not existingPlace:
            store = PlaceDocStore()
            return store.createOrUpdate(place)
        else:
            print("place exist")
            return existingPlace
    
    def getPlaceByMetaId(self, meta_id):
        print("======meta_id ")
        print(meta_id)
        store = PlaceDocStore()
        return store.get_by_meta_id(meta_id)
    