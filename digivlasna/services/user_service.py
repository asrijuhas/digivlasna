from digivlasna.core.documents.user import UserDoc
from digivlasna.stores.user_store import UserDocStore
from digivlasna.core.exceptions import EntityValidationException

class UserService():
    def __init__(self):
        self.default_params ={
            'id':None,
            'name':None,
            'platform': None,
            'screen_name': None,
            'created_at':None,
            'followers_count':None,
            'likes_count':None,
            'friends_count':None,
            'post_count':None,
            'geo_enabled':None,
            'location':None,
            'country':None,
            'country_code':None,
            'creawling_at':None
        }
    # def saveOrUpdateUser(userDoc):
    #     pass
    
    def saveOrUpdateUserFromDict(self, **kwargs):
        """
        save or update user from dictionary
        Args: 
            **kwargs key value attribute for UserDoc defined on self.default_params
        Returns:
            UserDoc: return value 
        Raises:
            digivlasna.core.exceptions.EntityValidationException
        """
        user = UserDoc()
        for item, default in self.default_params.items():
            setattr(user, item, kwargs.get(item,default))
        meta_id = UserDoc.get_meta_id(user.platform, user.id)
        existingUser = self.getUserByMetaId(meta_id)
        if not existingUser:
            store = UserDocStore()
            return store.createOrUpdate(user)
        else:
            print("user exist")
            return existingUser


    def getUserByMetaId(self, meta_id):
        store = UserDocStore()
        return store.get_by_meta_id(meta_id)


