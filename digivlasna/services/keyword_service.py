from digivlasna.core.models import SosmedKeyword, ClientSosmedKeyword
from django.db import transaction
from digivlasna.stores.digivla_store import SosmedKeywordDocStore
from digivlasna.core.documents.digivla import SosmedKeywordDoc

class SosmedKeywordService():
    def __init__(self, client_id):
        self.client_id = client_id
        
    @transaction.atomic
    def add_keyword(self, keyword_text):
        sosmedKeyword = SosmedKeyword.objects.filter(keyword__exact=keyword_text).first()
        store = SosmedKeywordDocStore()

        if not sosmedKeyword:
            #simpan data ke database
            sosmedKeyword = SosmedKeyword()
            sosmedKeyword.keyword = keyword_text
            sosmedKeyword.save()
            
            # simpan data ke elastic
            sosmedKeywordDoc = SosmedKeywordDoc()
            sosmedKeywordDoc.id = sosmedKeyword.id
            sosmedKeywordDoc.uuid = sosmedKeyword.uuid
            sosmedKeywordDoc.keyword=keyword_text
            sosmedKeywordDoc.client_count = sosmedKeyword.client_count
            sosmedKeywordDoc.created_at = sosmedKeyword.created_at
            store.create(sosmedKeywordDoc)
            
            clientSosmedKeyword = ClientSosmedKeyword()
            clientSosmedKeyword.client_id = self.client_id
            clientSosmedKeyword.keyword_id = sosmedKeyword.id
            clientSosmedKeyword.save()
        else:
            clientSosmedKeyword = ClientSosmedKeyword.objects.filter(client_id= self.client_id, keyword=sosmedKeyword).first()
            if not clientSosmedKeyword:
                clientSosmedKeyword = ClientSosmedKeyword()
                clientSosmedKeyword.keyword = sosmedKeyword
                clientSosmedKeyword.client_id = self.client_id
                clientSosmedKeyword.save()
        return sosmedKeyword

    
    def delete_keyword(self, keyword_text):
        sosmedKeyword = SosmedKeyword.objects.filter(keyword__exact=keyword_text).first()
        print("---delete keyword")
        print(keyword_text)
        print(sosmedKeyword)
        store = SosmedKeywordDocStore()

        if sosmedKeyword:
            clientSosmedKeyword = ClientSosmedKeyword.objects.filter(keyword=sosmedKeyword).first()
            #jika client sudah tidak ada menggunakan keyword ini maka baru boleh di hapus
            if not clientSosmedKeyword:
                uuid = sosmedKeyword.uuid
                sosmedKeyword.delete()
                store.delete(uuid)
        

            
        