from digivlasna.core.documents.digivla import TwitterAccountDoc
from digivlasna.core.utils.el import el_get_first_doc , el_is_exists

class AccountSelector(object):
    """ AccountSelector adalah object yang akan menentukan account apa yang akan digunakan
    untuk hit api  media social  misal berdasarkan paramater client_id,
    """
    def __init__(self, **kwargs):
        #modify params_defaults untuk menambahkan data / variabel yang dibutuhkan untuk
        self.params_default = {
            'client_id': None
        }

        for param, default in self.params_default.items():
            setattr(self,param, kwargs.get(param, default))
    
    def get_twitter_account(self):
        # untuk tujuan testing account di ambil yang pertama dari TwitterAccountDoc
        s = TwitterAccountDoc.search()
        r = s.execute()
        return el_get_first_doc(r)