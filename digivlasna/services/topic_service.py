from digivlasna.core.models import SosmedTopic, ClientSosmedKeyword, Client, SosmedKeyword, HiddenTopic
from digivlasna.core.documents.digivla import SosmedTopicDoc
from digivlasna.stores.digivla_store import SosmedTopicDocStore
from digivlasna.services.keyword_service import SosmedKeywordService
from digivlasna.core.utils import el
from digivlasna.core.exceptions import EntityNotFoundException
from django.db import transaction
from django.db import connection

class MedsosTopicService():

    def __init__(self, client_uuid):
        self.client = Client.objects.filter(uuid = client_uuid).first()
        self.client_id = self.client.pk
        self.store = SosmedTopicDocStore()

    def create_topic(self, topic_name, keywords_text):
        keywordService = SosmedKeywordService(self.client_id)
        sosmedKeywords =[]
        topic = SosmedTopic()
        topic.name = topic_name
        topic.client_id = self.client_id
        topic.save()

        #keyword yang akan disimpan di elastic
        topic_keywords = []
        keywords_text = [item.lower() for item in keywords_text]
        for keyword_text in keywords_text:
            sosmedKeyword = keywordService.add_keyword(keyword_text)
            topic_keywords.append(sosmedKeyword.keyword)
            topic.keywords.add(sosmedKeyword)
            topic.save()

        topicDoc = SosmedTopicDoc()
        topicDoc.id = topic.id
        topicDoc.uuid = topic.uuid
        topicDoc.topic = topic.name
        topicDoc.client_id = topic.client_id
        topicDoc.created_at = topic.created_at
        topicDoc.keyword = topic_keywords
        self.store.create(topicDoc)
        return topicDoc.to_dict()

    def update_topic(self,uuid, topic_name, keywords_text):
        topic = SosmedTopic.objects.filter(uuid=uuid, client_id = self.client_id).first()
        keywordService = SosmedKeywordService(self.client_id)

        #convert keyword to lowercase
        keywords_text = [item.lower() for item in keywords_text]

        if not topic :
            raise Exception("topic not found")
        topic.name = topic_name
        keywords_existing = []
        for db_keyword in topic.keywords.all():
            keywords_existing.append(db_keyword.keyword)
        keywords_todelete=[]
        for keyword_existing in keywords_existing:
            if keyword_existing not in keywords_text:
                keywords_todelete.append(keyword_existing)
        keywords_toadd = []
        for new_keyword in keywords_text:
            if new_keyword not in keywords_existing:
                keywords_toadd.append(new_keyword)

        #delete unused keyword
        topic.keywords.filter(keyword__in=keywords_todelete).delete()
        for keyword_todelete in keywords_todelete:
            keywordService.delete_keyword(keyword_todelete)
        for keyword_toadd in keywords_toadd:
            sosmedKeyword = keywordService.add_keyword(keyword_toadd)
            topic.keywords.add(sosmedKeyword)
            topic.save()

        topicDoc = SosmedTopicDoc()
        topicDoc.id = topic.id
        topicDoc.uuid = topic.uuid
        topicDoc.topic = topic.name
        topicDoc.client_id = topic.client_id
        topicDoc.created_at = topic.created_at
        topicDoc.keyword = keywords_text
        topicDoc = self.store.createOrUpdate(topicDoc)
        return topicDoc.to_dict()


    def client_topic_list(self, offset, count):
        if type(offset) == str:
            offset = int(offset)

        if type(count) == str:
            count=int(count)

        # resp = self.store.list(offset, count)
        resp = self.store.client_topics(self.client_id, offset, count)
        # topics = el.get_documents_dict(resp)
        topics = map(lambda doc: {'id':doc['_source']['id'], 'uuid': doc['_source']['uuid'],
                                  'topic': doc['_source']['topic'],
                                  'keywords': doc['_source']['keyword'] if 'keyword' in doc['_source'] else None
                                } , resp.to_dict()['hits']['hits'])
        total_topic = el.el_total_matched(resp)
        return {'total':total_topic,  'page':offset, 'size':count, 'topics':topics}
    def client_keywords(self,term, offset, count):

        if type(offset) != int:
            offset = int(offset)
        if type(count) != int:
            count = int(count)
        start = offset * count
        end = start + count
        print(term)
        if term and len(term) > 1:
            term="%%%s%%"%term
            q_total =(  "SELECT count(*) "
                        "FROM client_keywords ck "
                        "LEFT JOIN sosmed_keywords k on k.id =ck.keyword_id "
                        "WHERE ck.client_id={0} AND k.keyword LIKE '{1}' ;".format(self.client_id, term)
                    )
            query = (   "SELECT k.uuid, k.keyword, ck.client_id, ck.exclude_text "
                        "FROM client_keywords ck "
                        "LEFT JOIN sosmed_keywords k on k.id = ck.keyword_id "
                        "LEFT JOIN clients c on c.id = ck.client_id "
                        "WHERE ck.client_id={0} and k.keyword LIKE '{1}' "
                        "LIMIT {2} OFFSET {3}; ".format(self.client_id,term, end, start)
                    )
        else:
            q_total = " select count(*) from client_keywords where client_id = {0}  ; ".format(self.client_id)
            query = (   "SELECT k.uuid, k.keyword, ck.client_id, ck.exclude_text  "
                        "FROM client_keywords ck "
                        "LEFT JOIN sosmed_keywords k on k.id = ck.keyword_id "
                        "LEFT JOIN clients c on c.id = ck.client_id "
                        "WHERE ck.client_id={0}  "
                        "LIMIT {1} OFFSET {2}; ".format(self.client_id, end, start)
                    )
        print(query)
        total = 0
        client_keywords =[]
        with connection.cursor() as cursor:
            cursor.execute(q_total)
            row = cursor.fetchone()
            total = row[0]
        with connection.cursor() as cursor:
            cursor.execute(query)
            rows = cursor.fetchall()
            for row in rows:
                item ={'uuid':row[0], 'keyword':row[1], 'exclude_text':row[3]}
                client_keywords.append(item)
        data={'total':total, 'page':offset, 'size':count, 'keywords':client_keywords}
        return data



    def topic_detail(self, topic_uuid):
        doc = self.store.get(topic_uuid)
        if doc is None:
            raise EntityNotFoundException()
        print("topic detail ....", doc.client_id, " == ", self.client_id)
        if int(doc.client_id) != int(self.client_id):

            return None
        doc = doc.to_dict(include_meta=False)
        print("doc ",doc)
        if doc:
            return {'id':doc['id'], 'uuid':doc['uuid'], 'topic':doc['topic'], 'keyword':doc['keyword']}

    def is_keyword_used_by_client(self,keyword_id):
        topics = SosmedTopic.objects.filter(client_id=self.client_id )
        is_exist=False
        for topic in topics:
            if topic.keywords.filter(id=keyword_id).exists():
                is_exist = True
                break
        return is_exist


    @transaction.atomic
    def delete(self, topic_uuid):
        topics = SosmedTopic.objects.filter(uuid=topic_uuid, client_id = self.client_id)
        keywordService = SosmedKeywordService(self.client_id)
        keyword_ids = []
        for topic in topics:
            for keyword in topic.keywords.all():
                keyword_ids.append(keyword.pk)
                # keywordService.delete_keyword(keyword.keyword)
            topic.delete()
        for keyword_id in keyword_ids:
            is_used = self.is_keyword_used_by_client(keyword_id)
            if not is_used:
                clientKeywords = ClientSosmedKeyword.objects.filter(client_id=self.client_id, keyword_id = keyword_id)
                for clientKeyword in clientKeywords:
                    clientKeyword.delete()
        keywordService.delete_keyword(keyword.keyword)

        self.store.delete_with_client_id(topic_uuid, self.client_id)



    def create_keyword(self, keyword_text, exclude_text=None):
        keywordService = SosmedKeywordService(self.client_id)
        keyword_text = keyword_text.lower()
        sosmedKeyword = keywordService.add_keyword(keyword_text)
        clientKeyword = ClientSosmedKeyword.objects.filter(client_id = self.client_id, keyword_id = sosmedKeyword.id).first()
        if not clientKeyword:
            clientKeyword = ClientSosmedKeyword()
            clientKeyword.client = self.client
            clientKeyword.keyword = sosmedKeyword
            clientKeyword.exclude_text = exclude_text
            clientKeyword.save()
        else:
            if exclude_text:
                clientKeyword.exclude_text = exclude_text
                clientKeyword.save()

        data ={'uuid':sosmedKeyword.uuid, 'keyword':sosmedKeyword.keyword }
        return data
    def update_keyword(self, keyword_text, exclude_text=None):
        keyword_text = keyword_text.lower()
        sosmedKeyword = SosmedKeyword.objects.filter(keyword__exact = keyword_text).first()

        clientKeyword = ClientSosmedKeyword.objects.filter(client_id = self.client_id, keyword=sosmedKeyword).first()
        if clientKeyword:
            clientKeyword.exclude_text = exclude_text
            clientKeyword.save()
        return {'uuid':sosmedKeyword.uuid, 'keyword': sosmedKeyword.keyword}

    def delete_keyword(self, keyword_text):
        keywordService = SosmedKeywordService(self.client_id)
        clientKeyword = ClientSosmedKeyword.objects.filter(client_id = self.client_id, keyword__keyword=keyword_text).first()
        if clientKeyword:
            clientKeyword.delete()
            keywordService.delete_keyword(keyword_text)

    def detail_keyword(self, keyword_text):
        sosmedKeyword = SosmedKeyword.objects.filter(keyword__exact = keyword_text).first()
        clientKeyword = ClientSosmedKeyword.objects.filter(client_id = self.client_id, keyword__keyword=keyword_text).first()
        if clientKeyword and sosmedKeyword:
            return {'keyword':sosmedKeyword.keyword, 'exclude_text':clientKeyword.exclude_text}
        return None

    def create_hidden_topic(self, topic_text):
        hidden_topic = HiddenTopic.objects.filter(topic = topic_text).first()

        if not hidden_topic:
            hidden_topic = HiddenTopic()
            hidden_topic.topic = topic_text
            hidden_topic.save()

    def delete_hidden_topic(self, topic_text):
        hidden_topic = HiddenTopic.objects.filter(topic = topic_text).first()
        if hidden_topic:
            hidden_topic.delete()

    def list_hidden_topic(self):
        hidden_topic = HiddenTopic.objects.all()

        data = []
        if hidden_topic:
            for topic in hidden_topic:
                data_entity = {
                    'id': topic.id,
                    'uid': topic.uuid,
                    'topic': topic.topic
                }

                data.append(data_entity)

            return data

        return data
