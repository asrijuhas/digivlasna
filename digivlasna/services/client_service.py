from digivlasna.stores.digivla_store import ClientDocStore
from digivlasna.core.utils import el
from digivlasna.core.utils import random
from digivlasna.core.documents.digivla import ClientDoc
from digivlasna.core.models import Client
from digivlasna.core.exceptions import EntityNotFoundException
class ClientService():
    def __init__(self,identity):
        self.identity=identity
        self.doc_store = ClientDocStore()
        self.create_params = {
            'name':None,
            'client_code': random.random_string(10),
            'icon': None,
            'address':None,
            'phone': None,
            'contact_name': None,
            'email':None,
            'join_date': None,
            'expired_at': None,
            'status': None
        }

        self.update_params = {
            'name':None,
            'icon': None,
            'address':None,
            'phone': None,
            'contact_name': None,
            'email':None,
            'join_date': None,
            'expired_at': None,
            'status': None
        }
        
    
    def get_client(self,id):
        client = Client.objects.get(pk=id)
        client = self.doc_store.get(client.uuid)
        return client
    
    
    def update_client(self, client_id, **kwargs):
        
        client = Client.objects.filter(id = client_id).first()
        doc = ClientDoc()
        if not client:
            try:
                self.doc_store.delete_by_client_id(client_id)
            except Exception as ex:
                pass
        if not client:
            raise EntityNotFoundException()
            
        
        for (param, default) in self.update_params.items():
            setattr(client, param, kwargs.get(param, default))
            setattr(doc, param, kwargs.get(param, default))
        client.updated_by = self.identity.username
        doc.updated_by = self.identity.username
        client.save()
        doc.uuid = client.uuid
        doc.id = client.id
        return self.doc_store.update(doc).to_dict()
    
    def delete_client(self, client_id):
        client = Client.objects.filter(id=client_id).first()
        
        if client:
            self.doc_store.delete(client.uuid)
            client.delete()
        else:
            self.doc_store.delete_by_client_id(client_id)

        
    def create_client(self, **kwargs):
        client = Client()
        doc = ClientDoc()
        for (param, default) in self.create_params.items():
            setattr(client, param, kwargs.get(param, default))
            setattr(doc, param, kwargs.get(param, default))
        client.updated_by = self.identity.username
        client.created_by = self.identity.username
        doc.updated_by = self.identity.username
        doc.created_by = self.identity.username
        
        client.save()
        # client.name = clientDoc.name
        # client.client_code = clientDoc.client_code
        # client.icon = clientDoc.icon
        # client.address = clientDoc.address
        # client.phone = clientDoc.phone
        # client.contact_name = clientDoc.contact_name
        # client.join_date = clientDoc.join_date
        # client.expired_at = clientDoc.expired_at
        # client.status = clientDoc.status
        doc.uuid = client.uuid
        doc.id = client.id
        return self.doc_store.create(doc)
    
    
    def list_client(self,offset=0, count=10):
        if type(offset) == str:
            offset = int(offset)
        
        if type(count) == str:
            count=int(count)
        
        response = self.doc_store.list(offset,count)
        clients = el.get_documents_dict(response)
        total_client = el.el_total_matched(response)
        data ={'total':total_client, 'page':offset, 'size':count, 'clients':clients}
        return {'data':data , 'msg':'ok','code':200}
        
    
    # def create_topic(self, **kwargs):
    # @staticmethod
    # def list_by_status(status, offset=0, count=10):
    #     store = ClientDocStore()
    #     response = store.list_by_status(status, offset, count)
    #     clients = el.get_documents_dict(response)
    #     total_client = el.el_total_matched(response)
    #     return {'total':total_client, 'clients':clients, }
    # @staticmethod
    # def get_by_client_id(client_id):
    #     store = ClientDocStore()
    #     resp = store.get_by_client_id(client_id)
    #     if resp:
    #         return resp.to_dict()['hits']['hits'][0]['_source']#el.el_get_first_doc(resp.to_dict())
        