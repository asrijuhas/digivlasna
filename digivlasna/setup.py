from setuptools import setup
import os

def strip_comments(l):
    return l.split('#',1)[0].strip()

def reqs(*f):
    return list(filter(None, [strip_comments(l) for l in open(os.path.join(os.getcwd(),*f)).readlines()]))

setup(
    name="digivla_sna",
    version="0.0.1",
    description="digivla social network analysis"
    long_description="""
    DIGIVLA SNA social network analysis
    """
    install_requires=reqs('requirements.txt')
)