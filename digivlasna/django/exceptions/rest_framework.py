from rest_framework.views import exception_handler

def digivla_exception_handler(exc, context):
    resp = exception_handler(exc, context)
    if resp is not None:
        resp.data['code'] = resp.status_code
        resp.data['msg'] = "%s | %s" %(resp.status_text , resp.data['detail'])
        del resp.data['detail']

    return resp