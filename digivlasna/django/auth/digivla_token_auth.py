from rest_framework import authentication
from rest_framework import exceptions
from django.conf import settings 
from digivlasna.core.models.identity import Identity
from digivlasna.core.models import User
import requests
import base64
class DigivlaTokenAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        identityEndpoint = settings.IDENTITY_SERVICE_ENDPOINT
        if identityEndpoint is None:
            raise Exception("IDENTITY_SERVICE_ENDPOINT in settings not set")
        
        identityEndpoint ="%s/identity" %identityEndpoint
        print("IDENTITY ENDPOINT .......", identityEndpoint)
        auth = request.META.get('HTTP_AUTHORIZATION', b'')
        if not auth:
            raise exceptions.AuthenticationFailed("Cannot authenticate.")
        # auth = base64.b64decode(auth).decode('HTTP_HEADER_ENCODING').split(':')
        auth = auth.split()
        if len(auth) != 2:
            raise exceptions.AuthenticationFailed("schema auth tidak dimengerti ..")
        if auth[0] != 'Token':
            raise exceptions.AuthenticationFailed("schema auth tidak di mengerti")
        token = auth[1]

        resp =requests.post(url=identityEndpoint, json={'token':token})
        print("-------inside DigivlaTokenAuthentication---- resp -->:")
        print(resp, resp.text)
        json = resp.json()
        if  resp.status_code != 200:
            raise exceptions.AuthenticationFailed(json['msg'])
        print(json)
        identity = Identity(**json['data'])
        if not identity.username :
            raise exceptions.AuthenticationFailed()
        try:
            user = identity
        except User.DoesNotExist as ex:
            raise exceptions.AuthenticationFailed(str(ex))
        return (user, None)
        
            


