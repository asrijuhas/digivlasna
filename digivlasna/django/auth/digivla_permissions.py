from rest_framework.permissions import BasePermission
from digivlasna.core.constants import ROLE_NAME_ADMIN, ROLE_NAME_ANALYST,ROLE_NAME_CLIENT_USER

class SuperPermission(BasePermission):
    message = "you must admin."

    def has_permission(self, request, view):
        identity = request.user
        if identity.is_superuser:
            return True
        return False

class AdminPemission(BasePermission):
    def has_permission(self, request, view):
        identity = request.user
        if identity.is_superuser:
            return True
        if ROLE_NAME_ADMIN in identity.roles:
            return True
        return False

class ShowClientDataPermission(BasePermission):
    def has_object_permission(self, request, view, client_uuid):
        
        
        identity = request.user
        # if identity.is_superuser:
        #     return True
        
        # if ROLE_NAME_ADMIN in identity.roles:
        #     return True
        if not identity.client_uuid:
            return False
        if identity.client_uuid == client_uuid:
            return True
        
        # if client_uuid in identity.project_clients:
        #     return True
        return False

class AddKeywordPermission(BasePermission):
    def has_permission(self, request, view):
        identity = request.user
        if identity.is_superuser:
            return True
        if ROLE_NAME_ADMIN in identity.roles:
            return True
        return False

class ValidUserLoginPermission(BasePermission):
    def has_permission(self, request, view):
        identity = request.user
        if identity.is_superuser:
            return True
        if ROLE_NAME_ADMIN in identity.roles:
            return True
        if ROLE_NAME_ANALYST in identity.roles:
            return True
        if ROLE_NAME_CLIENT_USER in identity.roles:
            return True
        return False
        

    